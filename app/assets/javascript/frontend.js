

$(function() {
    var drawCanvas;
    var canvas = $("canvas");

    var imgNames = {
        "wall": "mountain.png",
        "exit": "exit.png",
        "grass": "grass.png",
        "player": "player.png"
    };

    var imgs = {

    };

    var board = [];
    var pos = {};

    var handleResponse = function(data) {
        console.log(data);

        board = data.board;
        pos = data.pos;

        //$("p").text(JSON.stringify(pos));

        drawCanvas();
    };

    var onImagesLoaded = function() {
        $.ajax("board").done(handleResponse);
    };

    document.onkeydown = function(event) {
        var keyMap = {
            '13': "reset",
            '37': "left",
            '38': "up",
            '39': "right",
            '40': "down",
        };

        if (event.keyCode in keyMap) {
            $.post("action", {"action": keyMap[event.keyCode]}).done(handleResponse);
        }
    };

    var toLoad = 0;
    for (var name in imgNames) {
        var img = new Image();
        img.src = "public/assets/images/" + imgNames[name];
        toLoad += 1;
        img.onload = function() {
            toLoad -= 1;

            if (toLoad == 0) {
                onImagesLoaded();
            }
        };
        imgs[name] = img;
    }

    var ctx = canvas[0].getContext("2d");

    drawCanvas = function() {
        ctx.fillRect(0,0,180,180);

        for (var i = 0; i < 9 && i < board.length; i++) {
            for (var j = 0; j < 9 && j < board[i].length; j++) {

                if (board[i][j] == 0) {
                    ctx.drawImage(imgs["grass"], j*20, i*20);
                } else if (board[i][j] == 1) {
                    ctx.drawImage(imgs["wall"], j*20, i*20);
                } else if (board[i][j] == 2) {
                    ctx.drawImage(imgs["grass"], j*20, i*20);
                    ctx.drawImage(imgs["exit"], j*20, i*20);
                }
            }
        }
        ctx.drawImage(imgs["player"], 4*20, 4*20);
    };
});