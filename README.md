# README #

ia-lab, a labyrinth witout end.

### Setup ###

```
sudo apt-get install nodejs
sudo npm install -g bower grunt
npm install
bower install
grunt copy
grunt concat
grunt less
```

### Run ###

```
node server.js
```

Open `localhost:2021`. 

After modifying a javascript or css file for the html page, run `grunt concat` or `grunt less` respectively.