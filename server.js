var express = require('express');
var path = require('path');
var bodyParser = require('body-parser')
var utils = require("./utils.js");
var config = require("./config.js");

var canMove = function(board, x, y) {
  if (x < 0 || y < 0 || x >= config.size || y >= config.size) {
    return false;
  }
  if (board[x][y] == 0 || board[x][y] == 2) {
    return true;
  }
  return false;
};

function canSolve(board, pos) {
  board = JSON.parse(JSON.stringify(board));

  var changed = true;
  var copy = "";

  while (copy != JSON.stringify(board)) {
    if (board[pos.x][pos.y] == 2) {
      return true;
    }
    
    copy = JSON.stringify(board);
    for (var i = 0; i < board.length; i+=1) {
      for (var j = 0; j < board[i].length; j += 1) {
        if (board[i][j] == 2) {
          if (canMove(board, i+1, j)) {
            board[i+1][j] = 2;
          }
          if (canMove(board, i, j-1)) {
            board[i][j-1] = 2;
          }
          if (canMove(board, i, j+1)) {
            board[i][j+1] = 2;
          }
          if (canMove(board, i-1, j)) {
            board[i-1][j] = 2;
          }
        }
      }
    }
  }

  return false;
}

function generateBoard() {
	/* 0 : grass, 1: wall, 2: exit, -1: emptiness */
	var game = Array.apply(null, Array(config.size*config.size)).map(function(){return 0});
	for (i = 0; i < config.size*config.size*config.walls; i += 1) {
		game[i] = 1;
	}

	game[game.length-1] = 2;
	game.shuffle();

	var board = [];

	for (i = 0; i < config.size; i++) {
		board.push(game.splice(0, config.size));
	}

	var playerPos = [config.size >> 1, config.size >> 1];

	while (board[playerPos[0]][playerPos[1]] != 0) {
		playerPos = [Math.floor(Math.random() * config.size), Math.floor(Math.random() * config.size)];		
	}

  //console.log("solving");
  if (canSolve(board, {"x": playerPos[0], "y": playerPos[1]})) {
    //console.log("ok board");
    return {"board": board, "pos": {"x": playerPos[0], "y": playerPos[1]}};
  } else {
    //console.log("can't solve");
    return generateBoard();
  }
}

var board = generateBoard();

displayBoard = function(board) {
  var vision = [];
  for (var i = -4; i < 5; i++) {
    var row = [];
    var x = board.pos.x + i;
    for (var j = -4; j < 5; j++) {
      var y = board.pos.y + j;
      if (x < 0 || y < 0 || x >= config.size || y >= config.size) {
        row.push(-1);
      } else {
        row.push(board.board[x][y]);
      }
    }
    vision.push(row);
  }

  return {"pos": {"row": board.pos.x, "col": board.pos.y}, "board": vision};
}

var app = express();

app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 

console.log(__dirname + '/public');
app.use("/public", express.static(__dirname + '/public'));

app.get("/", function(req, res) {
  res.render("index.kiwi");
});

app.get("/board", function(req, res) {
  res.json(displayBoard(board));
});

app.post("/action", function(req, res) {
  var dir = req.body.action;

  var b = board.board;
  var pos = board.pos;

  if (dir == "reset") {
    board = generateBoard();
  } else {
    if (dir == "up" && canMove(b, pos.x-1, pos.y)) {
      pos.x -= 1;
    } else if (dir == "left" && canMove(b, pos.x, pos.y-1)) {
      pos.y -= 1;
    } else if (dir == "down" && canMove(b, pos.x+1, pos.y)) {
      pos.x += 1;
    } else if (dir == "right" && canMove(b, pos.x, pos.y+1)) {
      pos.y += 1;
    }
    if (b[pos.x][pos.y] == 2) {
      board = generateBoard();
    }
  }

  res.json(displayBoard(board));
});

app.listen(config.web.port);